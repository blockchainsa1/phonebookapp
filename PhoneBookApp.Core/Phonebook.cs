﻿using System;

namespace PhoneBookApp.Core
{
    public class Phonebook
    {

        public Phonebook(int phonebookId, string phonebookName, PhonebookEntry[] entries)
        {
            this.PhonebookID = phonebookId;
            this.PhonebookName = phonebookName;
            this.Entries = entries;
        }

        public long PhonebookID { get; set; }

        public string PhonebookName { get; set; }

        public PhonebookEntry[] Entries { get; set; }
    }
}
