﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using PhoneBookApp.Core;

namespace PhonebookApp.Service.Controllers
{
    [EnableCors("LocalCors")]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class PhoneBookController : Controller
    {
        private IMongoCollection<PhonebookEntry> _phonebookEntries { get; set; } = null;
        private IMongoClient _mongoClient { get; set; } = null;

        [HttpGet("[action]")]
        public async Task<Phonebook[]> GetPhonebooks() => await Task.Run(async () =>
        {
            List<Phonebook> phonebooks = new List<Phonebook>();

            phonebooks.Add(new Phonebook(1, "Emergency Numbers", (await GetPhonebookEntries(1)).ToArray()));
            phonebooks.Add(new Phonebook(2, "Family Members", (await GetPhonebookEntries(2)).ToArray()));
            phonebooks.Add(new Phonebook(3, "Yellow Pages", (await GetPhonebookEntries(3)).ToArray()));
            phonebooks.Add(new Phonebook(4, "Business Directory", (await GetPhonebookEntries(4)).ToArray()));

            return phonebooks.ToArray();
        });

        [HttpPost("[action]")]
        public async void UpdatePhonebookEntry([FromForm]PhonebookEntry entry) => await Task.Run(async () =>
        {
            _mongoClient = new MongoClient("mongodb://localhost:27017");
            _phonebookEntries = _mongoClient.GetDatabase("phonebookApp").GetCollection<PhonebookEntry>("PhonebookEntries");
            await _phonebookEntries.ReplaceOneAsync(filter => filter.PhonebookEntryID == entry.PhonebookEntryID, entry);
        });

        [HttpPost("[action]")]
        public async void AddPhonebookEntry([FromForm]PhonebookEntry entry) => await Task.Run(async () =>
        {
            _mongoClient = new MongoClient("mongodb://localhost:27017");
            _phonebookEntries = _mongoClient.GetDatabase("phonebookApp").GetCollection<PhonebookEntry>("PhonebookEntries");
            await _phonebookEntries.InsertOneAsync(entry);
        });

        [HttpGet("[action]/{phonebookID}")]
        public async Task<PhonebookEntry[]> GetPhonebookEntries([FromRoute]long phonebookID) => await Task.Run(async () =>
        {
            _mongoClient = new MongoClient("mongodb://localhost:27017");
            _phonebookEntries = _mongoClient.GetDatabase("phonebookApp").GetCollection<PhonebookEntry>("PhonebookEntries");
            var filter = Builders<PhonebookEntry>.Filter.Eq(m => m.PhonebookID, phonebookID);
            var entries = (await _phonebookEntries.FindAsync(filter)).ToList();

            return entries.ToArray();
        });

        [HttpGet("[action]/{name}")]
        public async Task<PhonebookEntry[]> GetPhonebookEntriesMatchingName([FromRoute]string name) => await Task.Run(async () =>
        {
            _mongoClient = new MongoClient("mongodb://localhost:27017");
            _phonebookEntries = _mongoClient.GetDatabase("phonebookApp").GetCollection<PhonebookEntry>("PhonebookEntries");
            var filter = Builders<PhonebookEntry>.Filter.Where(f => f.Name.Contains(name));
            var entries = (await _phonebookEntries.FindAsync(filter)).ToList();

            return entries.ToArray();
        });
    }
}