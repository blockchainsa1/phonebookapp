
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FetchPhonebookDataComponent } from './fetch-phonebook-data/fetch-phonebook-data.component';
import { MatInputModule, MatTableModule, MatSelectModule } from '@angular/material';
import { HttpModule, BaseRequestOptions, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomRequestOptions extends BaseRequestOptions {
  constructor() {
    super();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Access-Control-Allow-Headers', 'Content-Type');
    this.headers.append('Access-Control-Allow-Origin', 'http://localhost:3252/');
  }
}

@NgModule({
  declarations: [
    AppComponent,
    FetchPhonebookDataComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    HttpModule
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    { provide: RequestOptions, useClass: CustomRequestOptions }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
