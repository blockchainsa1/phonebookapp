import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FetchPhonebookDataComponent } from './fetch-phonebook-data.component';

describe('FetchPhonebookDataComponent', () => {
  let component: FetchPhonebookDataComponent;
  let fixture: ComponentFixture<FetchPhonebookDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FetchPhonebookDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FetchPhonebookDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
