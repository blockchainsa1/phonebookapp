import { Component, OnInit, Inject } from '@angular/core';
import { Http, HttpModule, Response, Headers } from '@angular/http';
import { RequestOptions } from '@angular/http/src/base_request_options';


@Component({
  selector: 'app-fetch-phonebook-data',
  templateUrl: './fetch-phonebook-data.component.html',
  styleUrls: ['./fetch-phonebook-data.component.css']
})
export class FetchPhonebookDataComponent implements OnInit {
  public phonebooks: Phonebook[];
  public phonebookEntries: PhonebookEntry[];
  public phonebook_config: PhonebookConfig;

  constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {

    http.get(baseUrl + 'assets/phonebook.db.config.json').subscribe(res => {
      this.phonebook_config = res.json() as PhonebookConfig;
    });
  }

  public async getPhonebookEntries(phonebookID: number): Promise<IPhonebookEntry[]> {

    await this.http.get(this.phonebook_config.servicesUrl + 'api/phonebook/GetPhonebookEntries/' + phonebookID).subscribe(result => {
      this.phonebookEntries = result.json() as PhonebookEntry[];
    }, error => console.error(error));

    return this.phonebookEntries;
  }

  public async searchNumber(name: string): Promise<IPhonebookEntry[]> {
    let numbers: PhonebookEntry[] = [];

    await this.http.get(this.phonebook_config.servicesUrl + 'api/phonebook/GetPhonebookEntriesMatchingName/' + name).subscribe(result => {
      numbers = result.json() as PhonebookEntry[];
    }, error => console.error(error));

    return numbers;
  }

  public async addPhonebookEntry(entry: IPhonebookEntry): Promise<void> {
    await this.http.post(this.phonebook_config.servicesUrl + 'api/phonebook/AddPhonebookEntry/', JSON.stringify(entry)).subscribe(result => { }, error => console.error(error));
  }

  ngOnInit() {
    this.http.get(this.baseUrl + 'assets/phonebook.db.config.json').toPromise().then((res) => {
      this.phonebook_config = res.json() as PhonebookConfig;

      this.http.get(this.phonebook_config.servicesUrl + 'api/phonebook/GetPhonebooks').subscribe(res => {
        this.phonebooks = res.json() as Phonebook[];
      }, error => console.error(error));
    });
  }

}


export interface IPhonebookEntry {
  phonebookID: number;
  phonebookEntryID: number;
  name: string;
  phoneNumber: string;
}

export class Phonebook implements IPhonebook {
  constructor(public phonebookID: number, public phonebookName: string, public entries: IPhonebookEntry[]) { }
}

export class PhonebookEntry implements IPhonebookEntry {
  constructor(public phonebookID: number, public phonebookEntryID: number, public name: string, public phoneNumber: string) { }
}

export interface IPhonebook {
  phonebookID: number;
  phonebookName: string;
  entries: PhonebookEntry[];
}

export class PhonebookConfig {
  constructor(public servicesUrl: string) { }
}
